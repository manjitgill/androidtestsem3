package com.example.tappyspaceship01;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;
import java.util.Random;

public class GameEngine extends SurfaceView implements Runnable {

    // Android debug variables
    final static String TAG="DINO-RAINBOWS";

    // screen size
    int screenHeight;
    int screenWidth;


    // game state
    boolean gameIsRunning;

    // threading
    Thread gameThread;


    // drawing variables
    SurfaceHolder holder;
    Canvas canvas;
    Paint paintbrush;



    // -----------------------------------
    // GAME SPECIFIC VARIABLES
    // -----------------------------------
    Bitmap boundary1, boundary12, boundary2, boundary22, boundary3, boundary32, boundary4, boundary42;
        Player player;
        Item candy, rainbow, garbage, rainbow2;
    // ----------------------------
    // ## SPRITES
    // ----------------------------

    // represent the TOP LEFT CORNER OF THE GRAPHIC

    // ----------------------------
    // ## GAME STATS
    // ----------------------------
        int lives = 3;
        int score = 0;
        int numLoops = 0;
    public GameEngine(Context context, int w, int h) {
        super(context);

        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        this.screenWidth = w;
        this.screenHeight = h;

        this.printScreenInfo();

        this.player = new Player(getContext(), 2200, 200);
        this.candy = new Item(getContext(), 110, 250);
        this.rainbow = new Item(getContext(), 110, 370);
        this.rainbow2 = new Item(getContext(), 110, 130);
        this.garbage = new Item(getContext(),110,490);
    }



    private void printScreenInfo() {

        Log.d(TAG, "Screen (w, h) = " + this.screenWidth + "," + this.screenHeight);
    }

    private void spawnPlayer() {
        //@TODO: Start the player at the left side of screen
    }
    private void spawnEnemyShips() {
        Random random = new Random();

        //@TODO: Place the enemies in a random location

    }

    // ------------------------------
    // GAME STATE FUNCTIONS (run, stop, start)
    // ------------------------------
    @Override
    public void run() {
        while (gameIsRunning == true) {
            this.updatePositions();
            this.redrawSprites();
            this.setFPS();
        }
    }


    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            // Error
        }
    }

    public void startGame() {
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }


    // ------------------------------
    // GAME ENGINE FUNCTIONS
    // - update, draw, setFPS
    // ------------------------------

    public void updatePositions() {
        // to set a boundary for player to not leave the screen

            if (this.fingerAction == "mousedown")
            {
                Log.d(TAG, fingerAction);
                player.setyPosition(player.getyPosition() - 120);
                fingerAction = "aaaa";
                player.updateHitbox();
            }
            else if (this.fingerAction == "mouseup") {
                // if mouseup, then move player down
                player.setyPosition(player.getyPosition() + 120);
                fingerAction = "aaaa";
                player.updateHitbox();
            }

        if (numLoops % 5  == 0) {
                candy.setxPosition(candy.getxPosition() + 10);
                rainbow.setxPosition(rainbow.getxPosition() + 15);
            rainbow2.setxPosition(rainbow.getxPosition() + 15);
                garbage.setxPosition(garbage.getxPosition() + 20);

        }

      //----------------------------------------------------//
            if (candy.getxPosition() > screenWidth)
            {
                this.candy.setxPosition(110);
            }

            if (this.candy.getHitbox().intersect(player.getHitbox()) == true)
            {
               score  = score + 1;
                this.candy.setxPosition(110);
                this.candy.updateHitbox();
            }
            //--------------------------------------------------//
            if (rainbow.getxPosition() > screenWidth)
            {
                this.rainbow.setxPosition(110);

        if (this.rainbow.getHitbox().intersect(player.getHitbox()) == true)
        {
            score  = score + 1;
            this.rainbow.setxPosition(110);
            this.rainbow.updateHitbox();
        }
        }
        //--------------------------------------------------//
        if (rainbow2.getxPosition() > screenWidth)
        {
            this.rainbow2.setxPosition(110);

            if (this.rainbow2.getHitbox().intersect(player.getHitbox()) == true)
            {
                score  = score + 1;
                this.rainbow2.setxPosition(110);
                this.rainbow2.updateHitbox();
            }
        }
            //-------------------------------------------------//
            if (garbage.getxPosition() > screenWidth)
            {
                this.garbage.setxPosition(110);

            }
        if (this.garbage.getHitbox().intersect(player.getHitbox()) == true)
        {
            lives = lives - 1;

            this.garbage.setxPosition(110);
            this.garbage.updateHitbox();
        }

        int random = new Random().nextInt(4);

        }



    public void redrawSprites() {
        if (this.holder.getSurface().isValid()) {
            this.canvas = this.holder.lockCanvas();

            //----------------

            // configure the drawing tools
            this.canvas.drawColor(Color.argb(255, 255, 255, 255));
            paintbrush.setColor(Color.WHITE);


             boundary1  = BitmapFactory.decodeResource(this.getContext().getResources(), R.drawable.alien_laser);
            canvas.drawBitmap(boundary1, 100, screenHeight/8 + 50, paintbrush);boundary12  = BitmapFactory.decodeResource(this.getContext().getResources(), R.drawable.alien_laser);
            canvas.drawBitmap(boundary12, boundary1.getWidth() + 100, screenHeight/8 + 50, paintbrush);


            boundary2  = BitmapFactory.decodeResource(this.getContext().getResources(), R.drawable.alien_laser);
            canvas.drawBitmap(boundary2, 100, screenHeight/5 + 50, paintbrush);
             boundary22  = BitmapFactory.decodeResource(this.getContext().getResources(), R.drawable.alien_laser);
            canvas.drawBitmap(boundary22, boundary1.getWidth() + 100, screenHeight/5 + 50, paintbrush);

             boundary3  = BitmapFactory.decodeResource(this.getContext().getResources(), R.drawable.alien_laser);
            canvas.drawBitmap(boundary2, 100, screenHeight/3 - 30, paintbrush);
             boundary32  = BitmapFactory.decodeResource(this.getContext().getResources(), R.drawable.alien_laser);
            canvas.drawBitmap(boundary32, boundary1.getWidth() + 100, screenHeight/3 - 30, paintbrush);

             boundary4  = BitmapFactory.decodeResource(this.getContext().getResources(), R.drawable.alien_laser);
            canvas.drawBitmap(boundary4, 100, screenHeight/3 + 80, paintbrush);
             boundary42  = BitmapFactory.decodeResource(this.getContext().getResources(), R.drawable.alien_laser);
            canvas.drawBitmap(boundary42, boundary1.getWidth() + 100, screenHeight/3 + 80, paintbrush);


            // draw player graphic on screen
            canvas.drawBitmap(player.getImage(), player.getxPosition(), player.getyPosition(), paintbrush);
            // draw the player's hitbox
            canvas.drawRect(player.getHitbox(), paintbrush);
            // DRAW THE ITEM and its  HITBOX

            canvas.drawBitmap(candy.getImage(),candy.getxPosition(),candy.getyPosition(), paintbrush);
            candy.setImage(BitmapFactory.decodeResource(this.getContext().getResources(), R.drawable.candy32));
            canvas.drawRect(candy.getHitbox(), paintbrush);

            canvas.drawBitmap(rainbow.getImage(),rainbow.getxPosition(),rainbow.getyPosition(), paintbrush);
            rainbow.setImage(BitmapFactory.decodeResource(this.getContext().getResources(), R.drawable.rainbow32));
            canvas.drawRect(rainbow.getHitbox(), paintbrush);

            canvas.drawBitmap(rainbow2.getImage(),rainbow2.getxPosition(),rainbow2.getyPosition(), paintbrush);
            rainbow2.setImage(BitmapFactory.decodeResource(this.getContext().getResources(), R.drawable.rainbow32));
            canvas.drawRect(rainbow2.getHitbox(), paintbrush);


            canvas.drawBitmap(garbage.getImage(),garbage.getxPosition(),garbage.getyPosition(), paintbrush);
            garbage.setImage(BitmapFactory.decodeResource(this.getContext().getResources(), R.drawable.poop32));
            canvas.drawRect(garbage.getHitbox(), paintbrush);
            // ------------------------
            // 1. change the paintbrush settings so we can see the hitbox
            paintbrush.setColor(Color.BLUE);
            paintbrush.setStyle(Paint.Style.STROKE);
            paintbrush.setStrokeWidth(5);


            // DRAW GAME STATS
            // -----------------------------
            paintbrush.setColor(Color.BLACK);
            paintbrush.setTextSize(60);
            canvas.drawText("Lives remaining: " + lives,
                    500,
                    800,
                    paintbrush
            );


            canvas.drawText("Score: " + score,
                    500,
                    900,
                    paintbrush
            );



            //----------------
            this.holder.unlockCanvasAndPost(canvas);
        }
    }

    public void setFPS() {
        try {
            gameThread.sleep(50);
        }
        catch (Exception e) {

        }
    }

    // ------------------------------
    // USER INPUT FUNCTIONS
    // ------------------------------


    String fingerAction = "";

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int userAction = event.getActionMasked();
        //@TODO: What should happen when person touches the screen?
        if (event.getY() > screenHeight/2)
        {
            fingerAction = "mousedown";
        }
        else

        {
            fingerAction = "mouseup";
        }

//        if (userAction == MotionEvent.ACTION_DOWN) {
//            fingerAction = "mousedown";
//
//        }
//        else if (userAction == MotionEvent.ACTION_UP) {
//            fingerAction = "mouseup";
//        }

        return true;
    }
}
